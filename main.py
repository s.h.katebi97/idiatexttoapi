from Str2API import Str2API
import xlrd


def is_number(string: str) -> tuple:
    if '.' in string:
        return string.split('.')[0].isdigit() and string.split('.')[1].isdigit(), float
    else:
        return string.isdigit(), int


def to_number(string: str):
    is_digit, digit_type = is_number(string)
    if is_digit:
        return digit_type(string)
    else:
        return None


def initiate():
    workbook = xlrd.open_workbook('list.xlsx')
    names = ['لیست ورزش ها', 'لیست انسولین ها', 'لیست غذا ها', 'لیست دارو ها']
    drugs: list = []
    foods: list = []
    sports: list = []
    insulin: list = []
    for name in names:
        sheet = workbook.sheet_by_name(name)
        if name == 'لیست ورزش ها':
            sports = [sheet.cell_value(row, 1) for row in range(1, sheet.nrows)]
        elif name == 'لیست انسولین ها':
            insulin = [sheet.cell_value(row, 1) for row in range(1, sheet.nrows)]
        elif name == 'لیست غذا ها':  # 1271
            foods = [sheet.cell_value(row, 0) for row in range(1, sheet.nrows) if
                     '،' not in sheet.cell_value(row, 0) and '/' not in sheet.cell_value(row, 0) and '(' not in sheet.cell_value(
                         row, 0) and ')' not in sheet.cell_value(row, 0)]
        elif name == 'لیست دارو ها':
            drugs = [sheet.cell_value(row, 0).replace('/', ' ').replace('قرص ', '').replace('میلی', ' میلی') for row in
                     range(1, sheet.nrows)]
            for j in range(len(drugs)):
                numbers = [str(to_number(i)) for i in drugs[j].split() if is_number(i)[0]]
                if len(numbers) > 0:
                    index = drugs[j].rfind(numbers[-1]) - 1
                else:
                    index = len(drugs[j]) - 1
                drugs[j] = drugs[j][:index]
    return Str2API(drugs, foods, ['انسولین'], sports)


if __name__ == '__main__':
    str2api = initiate()

    food_example = ' صبح خورشت قیمه خوردم'
    drug_example = 'ٌنصف سینوریپا 5 خوردم'
    sport_example = 'صبح 20 دقیقه دویدم'
    insulin_example = 'برای ناهار 5 واحد انسولین کوتاه اثر میزنم'

    food_API = str2api.get_api(food_example)
    drug_API = str2api.get_api(drug_example)
    sport_API = str2api.get_api(sport_example)
    insulin_API = str2api.get_api(insulin_example)

    print(True if food_API[0] == 1 else False)
    print(True if drug_API[0] == 0 else False)
    print(True if sport_API[0] == 3 else False)
    print(True if insulin_API[0] == 2 else False)
    print("\n")

    print('\n'.join(list(map(lambda x: str(x), food_API[1]))), '\n***********************')
    print('\n'.join(list(map(lambda x: str(x), drug_API[1]))), '\n***********************')
    print('\n'.join(list(map(lambda x: str(x), sport_API[1]))), '\n***********************')
    print('\n'.join(list(map(lambda x: str(x), insulin_API[1]))), '\n***********************')

