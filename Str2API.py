from typing import List
from constants import *
from datetime import datetime, timedelta


class Str2API:
    def __init__(self, drugs: List[str], foods: List[str], insulins: List[str], sports: List[str]):
        self.__string: str
        self.__lists: List[list]
        self.__string_split: str
        # __foods: List[str] = []
        # for food in foods:
        #     if '/' in food:
        #         __food = self.split(food)
        #         __f_food: List[str] = []
        #         __s_food: List[str] = []
        #         for parts in __food:
        #             __parts = self.split(parts, '/')
        #             if len(__parts) > 1:
        #                 __f_food.append(__parts[0])
        #                 __s_food.append(__parts[1])
        #             else:
        #                 __f_food.append(__parts[0])
        #                 __s_food.append(__parts[0])
        #         __foods.append(' '.join(__f_food))
        #         __foods.append(' '.join(__s_food))
        #     else:
        #         __foods.append(food)
        # foods = list(map(lambda food: str(food).replace('،', ''), foods))
        # foods = list(map(lambda food: str(food).replace('،', ''), foods))
        self.__lists = [drugs, foods, insulins, sports]
        self.__string_split = ''
        self.__string = ""

    @staticmethod
    def split(string: str, sep=None) -> List[str]:
        return string.split(sep)

    @staticmethod
    def get_numbers(string: str) -> list:
        return list(map(lambda num: int(num), (filter(lambda s: str(s).isdigit(), string.split()))))

    #  Context Recognition And Parse Functions  #

    @staticmethod
    def is_of_type(string: str, of_type: List[str]) -> bool:
        result = False
        for example in of_type:
            try:
                result |= example in string
            except TypeError:
                print(example, '|', string)
                raise TypeError
            if result:
                break
        return result

    @staticmethod
    def get_elem_of_type(string: str, of_type: list) -> list:
        result = []
        if isinstance(of_type[0], tuple):
            for example in of_type:
                if example[0] in string:
                    result.append(example[1])
        else:
            for example in of_type:
                if example in string:
                    result.append(example)
        return result

    @staticmethod
    def get_pos_of_type(string: str, pos_type: list, out_type) -> list:
        result = []
        string_splited = Str2API.split(string)
        if isinstance(pos_type[0], tuple):
            for example in pos_type:
                if ' ' in example[0]:
                    idx = string.find(example[0])
                    if idx != -1:
                        try:
                            if example[1] > 0:
                                val = out_type(Str2API.split(string[idx + len(example[0]):])[example[1]])
                                result.append(example[2] * val)
                            elif example[1] < 0:
                                val = out_type(Str2API.split(string[:idx])[example[1]])
                                result.append(example[2] * val)
                            else:
                                result.append(out_type(0))
                        except ValueError:
                            print('[!]', example, ',', string, ',', idx, ',', len(string))
                            raise ValueError
                else:
                    string_splited = Str2API.split(string)
                    for i in range(len(string_splited)):
                        if example[0] == string_splited[i]:
                            try:
                                result.append(example[2] * out_type(string_splited[i+example[1]]))
                            except ValueError:
                                print('[!!]', example[0], ',', string_splited, ',', i)
        else:
            for example in pos_type:
                for i in range(len(string_splited)):
                    if example in string_splited[i]:
                        result.append(out_type(string_splited[i-1]))
        return result

    def parse_food(self, string: str) -> tuple:
        food = self.get_elem_of_type(string, self.__lists[1])
        food = food[0] if len(food) != 0 else None

        amount = self.get_numbers(string)
        amount = amount[0] if len(amount) != 0 else None

        meal = self.get_elem_of_type(string, meals)
        meal = meal[0] if len(meal) != 0 else None

        time_included = True
        time_period = self.get_elem_of_type(string, time_periods)
        time_period = time_period[0] if len(time_period) != 0 else None
        time_offset = self.get_pos_of_type(string, time_offsets, int)
        time_offset = time_offset[0] if len(time_offset) != 0 else None
        if time_offset is None:
            if time_period is None:
                abs_time = datetime.now()
                time_included = False
            else:
                abs_time = time_period
        else:
            abs_time = datetime.now() + timedelta(minutes=time_offset)

        approximate_time = self.get_elem_of_type(string, approximate_times)
        approximate_time = approximate_time[0] if len(approximate_time) != 0 else not time_included

        return food, amount, meal, abs_time, approximate_time

    def parse_sport(self, string: str) -> tuple:
        sport = self.get_elem_of_type(string, self.__lists[3])
        sport = sport[0] if len(sport) != 0 else None

        duration = self.get_pos_of_type(string, durations, int)
        duration = duration[0] if len(duration) != 0 else None

        time_included = True
        time_period = self.get_elem_of_type(string, time_periods)
        time_period = time_period[0] if len(time_period) != 0 else None
        time_offset = self.get_pos_of_type(string, time_offsets, int)
        time_offset = time_offset[0] if len(time_offset) != 0 else None
        if time_offset is None:
            if time_period is None:
                abs_time = datetime.now()
                time_included = False
            else:
                abs_time = time_period
        else:
            abs_time = datetime.now() + timedelta(minutes=time_offset)

        approximate_time = self.get_elem_of_type(string, approximate_times)
        approximate_time = approximate_time[0] if len(approximate_time) != 0 else not time_included

        return sport, duration, abs_time, approximate_time

    def parse_insulin(self, string: str) -> tuple:
        insulin_type = self.get_elem_of_type(string, insulin)
        insulin_type = insulin_type[0] if len(insulin_type) != 0 else None

        amount = self.get_pos_of_type(string, unit, int)
        amount = amount[0] if len(amount) != 0 else None

        time_included = True
        time_period = self.get_elem_of_type(string, time_periods)
        time_period = time_period[0] if len(time_period) != 0 else None
        time_offset = self.get_pos_of_type(string, time_offsets, int)
        time_offset = time_offset[0] if len(time_offset) != 0 else None
        if time_offset is None:
            if time_period is None:
                abs_time = datetime.now()
                time_included = False
            else:
                abs_time = time_period
        else:
            abs_time = datetime.now() + timedelta(minutes=time_offset)

        ins_meal = self.get_elem_of_type(string, meals)
        ins_meal = ins_meal[0] if len(ins_meal) != 0 else None

        approximate_time = self.get_elem_of_type(string, approximate_times)
        approximate_time = approximate_time[0] if len(approximate_time) != 0 else not time_included

        return insulin_type, amount, abs_time, ins_meal, approximate_time

    def parse_drug(self, string: str) -> tuple:
        drug = self.get_elem_of_type(string, self.__lists[0])
        drug = drug[0] if len(drug) != 0 else None

        amount = self.get_elem_of_type(string, amounts)
        amount = amount[0] if len(amount) != 0 else 1

        time_included = True
        time_period = self.get_elem_of_type(string, time_periods)
        time_period = time_period[0] if len(time_period) != 0 else None
        time_offset = self.get_pos_of_type(string, time_offsets, int)
        time_offset = time_offset[0] if len(time_offset) != 0 else None
        if time_offset is None:
            if time_period is None:
                abs_time = datetime.now()
                time_included = False
            else:
                abs_time = time_period
        else:
            abs_time = datetime.now() + timedelta(minutes=time_offset)

        approximate_time = self.get_elem_of_type(string, approximate_times)
        approximate_time = approximate_time[0] if len(approximate_time) != 0 else not time_included

        return drug, amount, abs_time, approximate_time

    def get_api(self, string: str) -> tuple:
        if self.is_of_type(string, self.__lists[0]):
            # Drugs
            return 0, self.parse_drug(string)
        elif self.is_of_type(string, self.__lists[1]):
            # Foods
            return 1, self.parse_food(string)
        elif self.is_of_type(string, self.__lists[2]):  # self.__lists[2]):
            # Insulin
            return 2, self.parse_insulin(string)
        elif self.is_of_type(string, self.__lists[3]):
            # Sports
            return 3, self.parse_sport(string)
        else:
            print(string)
            raise ValueError("The passed string does not match with any valid context type")
